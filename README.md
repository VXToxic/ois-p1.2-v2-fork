# P1.2 → V2 Uvod v HTML in CSS

V pripravah na vaje V2 bo predstavljen kratek uvod v:

* **HTML** (prosto besedilo, oznake, atributi in elementi, naslov strani, odstavki, naslovi, seznami, povezave, slike, tabele, obrazci, SPAN in DIV, META oznake, napredne tabele),
* **CSS** (načini vpeljave slogov, selektorji, lastnosti in vrednosti, CLASS in ID selektorji, grupiranje in gnezdenje, psevdo razredi).